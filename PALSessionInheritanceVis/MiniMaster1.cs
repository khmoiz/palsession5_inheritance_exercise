﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PALSessionInheritanceVis
{
    // TODO: Make this class inherit from MasterOfTheHouse!
    public class MiniMaster1
    {
        // TODO: Declare a string named _name, and give your MiniMaster a name.
        
        // TODO: Declare an int named _learnedClasses, and give your MiniMaster a number less than 87.
        

        // TODO: Make this constructor pass the integer '1' through the inherited base constructor, as a parameter.
        public MiniMaster1()
        {
            Console.WriteLine($"My name is {_name}.");
            Console.WriteLine($"I have learned {_learnedClasses} classes.");

            Console.WriteLine("It is time to run the function.");

            TheFunction();

            // TODO: Call the base class' RunThroughColors function!
            
        }

        // TODO: Make this function override and use its own version of TheFunction()!
        public void TheFunction()
        {
            Console.WriteLine($"I am but a mini master in training.");
        }
    }
}
